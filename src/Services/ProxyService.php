<?php

namespace Otus\Services;


use Otus\Exeptions\EmptyProxyList;
use Otus\Exeptions\FailedLoadProxyFile;
use Otus\Interfaces\ProxyInterface;

class ProxyService implements ProxyInterface
{
    private $configService;
    private $enabled;
    private $proxyListFileName;
    private $ipArray;
    private $port;
    private $login;
    private $password;

    /**
     * ProxyService constructor.
     * @param $configService
     */
    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
    }

    /**
     *
     */
    public function init(): void
    {
        $this->enabled = $this->configService->getParam("enabled", "proxy");
        $this->proxyListFileName = $this->configService->getParam("fileName", "proxy");
        $this->port = $this->configService->getParam("port", "proxy");
        $this->login = $this->configService->getParam("login", "proxy");
        $this->password = $this->configService->getParam("password", "proxy");

        if ($this->isProxyEnabled()) {
            $this->loadProxyList();
        }
    }

    /**
     * @throws EmptyProxyList
     * @throws FailedLoadProxyFile
     */
    private function loadProxyList(): void
    {
        try {
            $this->ipArray = file($this->proxyListFileName, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        } catch (\Exception $e) {
            throw new FailedLoadProxyFile(sprintf('Failed load proxy file name: %s', $this->proxyListFileName));
        }

        if (empty($this->ipArray)) {
            throw new EmptyProxyList('Empty Proxy List');
        }
    }

    /**
     * @return bool
     */
    public function isProxyEnabled(): bool
    {
        return (bool)$this->enabled;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return (\is_array($this->ipArray) && !empty($this->ipArray)) ? $this->ipArray[array_rand($this->ipArray)] : '';
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}