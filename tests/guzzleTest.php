<?php

use GuzzleHttp\Client;

require_once __DIR__ . '/../bootstrap/autoload.php';

$client = new GuzzleHttp\Client();
$res = $client->request('GET', 'https://instantdomainsearch.com/#search=veruyu.xyz', [
    'auth' => ['user', 'pass']
]);
echo $res->getStatusCode();
// "200"
echo $res->getHeader('content-type');
// 'application/json; charset=utf8'
echo $res->getBody();
// {"type":"User"...']);