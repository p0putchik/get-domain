<?php

namespace Otus\Controllers;

use Otus\Core\Response;
use Otus\Interfaces\ControllerInterface;
use Otus\Interfaces\RequestInterface;
use Otus\Interfaces\ResponseInterface;
use Otus\Services\DomainHandlerService;

class DomainHandlerController implements ControllerInterface
{
    private $service;

    /**
     * DomainHandlerController constructor.
     * @param DomainHandlerService $service
     * @internal param LoggerService $logger
     */
    public function __construct(DomainHandlerService $service)
    {
        $this->service = $service;
    }

    /**
     * Executes request and returns response
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws \Otus\Exceptions\GetConfigParamException
     */
    public function execute(RequestInterface $request): ResponseInterface
    {
        $domain = $request->getParam('domain') ?? $this->service->getDomainForHandling();

        if (empty($domain)){
            exit('Empty domain queue');
        }

        $result = $this->service->checkAndRegisterDomain($domain);
        $response = new Response(sprintf('Try register domain %s, result: %s ', $domain, $result));

        return $response;
    }
}