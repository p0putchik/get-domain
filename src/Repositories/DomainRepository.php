<?php

namespace Otus\Repositories;

use Otus\Core\RabbitMQ;
use Otus\Interfaces\DomainRepositoryInterface;

class DomainRepository implements DomainRepositoryInterface
{
    private $rabbitMQ;
    private $domainName;
    private $startDateTime;
    private $endDateTime;

    /**
     * DomainRepository constructor.
     * @param $rabbitMQ
     */
    public function __construct(RabbitMQ $rabbitMQ)
    {
        $this->rabbitMQ = $rabbitMQ;
    }

    public function addDomain(string $domainName, string $startDateTime, string $endDateTime)
    {
        $sendMessageArray = [
            'domain' => $domainName,
            'startDateTime' => $startDateTime,
            'endDateTime' => $endDateTime
        ];

        $this->rabbitMQ->sendMessage(json_encode($sendMessageArray));
    }

    /**
     * @return bool
     */
    public function receiveDomainInfo(): bool
    {
        $this->rabbitMQ->receiveMessage();

        $rawDomainInfo = $this->rabbitMQ->getMessageBody();

        if (empty($rawDomainInfo)) {
            return false;
        }

        $domainInfo = json_decode($rawDomainInfo);

        $this->domainName = !empty($domainInfo->domain) ? $domainInfo->domain : null;
        $this->startDateTime = !empty($domainInfo->startDateTime) ? $domainInfo->startDateTime : null;
        $this->endDateTime = !empty($domainInfo->endDateTime) ? $domainInfo->endDateTime : null;

        if (empty($this->domainName)) {
            return false;
        }

        return true;
    }

    /**
     *
     */
    public function ackReceiveDomain()
    {
        $this->rabbitMQ->ackMessage();
    }

    /**
     *
     */
    public function closeConnection(): void
    {
        $this->rabbitMQ->closeConnectionRabbitMQ();
    }

    /**
     * @return mixed
     */
    public function getDomainName():string
    {
        return $this->domainName;
    }

    /**
     * @return mixed
     */
    public function getStartDateTime():string
    {
        return $this->startDateTime;
    }

    /**
     * @return mixed
     */
    public function getEndDateTime():string
    {
        return $this->endDateTime;
    }
}