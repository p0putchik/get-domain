<?php

namespace Otus\DomainAvailabilityChecker;

use Helge\Client\SimpleWhoisClient;
use Helge\Client\WhoisClientInterface;
use Otus\DomainAvailabilityChecker\Socks5SocketClient\Client;

class ExtendWhoIsClient extends SimpleWhoisClient implements WhoisClientInterface
{
    protected $socks5Client;

    /**
     * ExtendWhoIsClientsClient constructor.
     * @param Client $socks5Client
     * @internal param bool $server
     * @internal param int $port
     */
    public function __construct(Client $socks5Client/*, $server = false, $port = 43*/)
    {
        parent::__construct();
        $this->socks5Client = $socks5Client;
    }

    /**
     * @param string $domain
     * @return bool
     * @throws Socks5SocketClient\Exceptions\IOException
     * @throws Socks5SocketClient\Exceptions\SSLException
     * @throws Socks5SocketClient\Exceptions\Socks5AuthException
     * @throws Socks5SocketClient\Exceptions\Socks5ConnectionException
     * @throws Socks5SocketClient\Exceptions\Socks5Exception
     * @internal param $string $
     * domain the domain name to get whois data for
     */
    public function query($domain)
    {
        // Initialize the response to null
        $response = null;

        $this->socks5Client->connect($this->server, $this->port);
        $request = self::formatQueryString($domain);
        $this->socks5Client->send($request);
        $response = $this->socks5Client->readAll();
        $this->socks5Client->disconnect();

        if (empty($response)) {
            return false;
        }

        // return the response, even if we never sent a request
        $this->response = $response;

        return true;
    }

    public static function formatQueryString($queryString)
    {
        $temp = strtolower($queryString);
        $temp = trim($temp);

        // Format the domain query according to RFC3912
        return $temp . "\r\n";
    }
}