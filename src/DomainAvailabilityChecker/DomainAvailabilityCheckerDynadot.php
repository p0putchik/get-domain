<?php

namespace Otus\DomainAvailabilityChecker;

use Otus\Interfaces\DomainAvailabilityCheckerInterface;
use Otus\Interfaces\DomainRegistratorInterface;
use Otus\Services\ConfigService;

class DomainAvailabilityCheckerDynadot implements DomainAvailabilityCheckerInterface
{
    protected $configService;
    protected $registratorDynadot;

    /**
     * DomainAvailabilityCheckerDynadot constructor.
     * @param ConfigService $configService
     * @param DomainRegistratorInterface $registratorNamecheap
     */
    public function __construct(ConfigService $configService, DomainRegistratorInterface $registratorDynadot)
    {
        $this->configService = $configService;
        $this->registratorDynadot = $registratorDynadot;
    }

    /**
     * @throws \Otus\Exceptions\GetConfigParamException
     */
    public function init(): void
    {
        $paramsArray = [
            'apiKey' => $this->configService->getParam('apiKey', 'dynadot'),
            'apiHost' => $this->configService->getParam('apiHost', 'dynadot')
        ];

        $this->registratorDynadot->init($paramsArray);
    }

    /**
     * @param string $domain
     * @return bool
     */
    public function isAvailable(string $domain): bool
    {
        return $this->registratorDynadot->domainCheckAvailability($domain);
    }
}