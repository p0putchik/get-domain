<?php

namespace Otus\Interfaces;

interface DomainRegistratorInterface
{
    public function domainRegister(string $domain): bool;
    public function domainCheckAvailability(string $domain): bool;
}
