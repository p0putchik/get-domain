<?php

namespace Otus\DomainRegister;


use Otus\DomainRegister\Registrators\DomainRegistratorNamecheap;
use Otus\Interfaces\DomainRegisterInterface;
use Otus\Services\ConfigService;

class DomainRegisterNamecheap implements DomainRegisterInterface
{
    protected $configService;
    protected $registratorNamecheap;

    /**
     * DomainAvailabilityCheckerDynadot constructor.
     * @param ConfigService $configService
     * @param DomainRegistratorNamecheap $registratorNamecheap
     */
    public function __construct(ConfigService $configService, DomainRegistratorNamecheap $registratorNamecheap)
    {
        $this->configService = $configService;
        $this->registratorNamecheap = $registratorNamecheap;
    }

    /**
     * @param bool $isSandbox
     * @throws \Otus\Exceptions\GetConfigParamException
     */
    public function init(bool $isSandbox = false): void
    {
        $configDir = $isSandbox ? 'namecheapSandbox' : 'namecheap';

        $paramsArray = [
            'apiKey' => $this->configService->getParam('apiKey', $configDir),
            'apiUserName' => $this->configService->getParam('apiUserName', $configDir),
            'userName' => $this->configService->getParam('userName', $configDir),
            'clientIp' => $this->configService->getParam('clientIp', $configDir),
            'sandbox' => $isSandbox,
        ];

        $this->registratorNamecheap->init($paramsArray);
    }

    public function registerDomain(string $domain): bool
    {
        return $this->registratorNamecheap->domainRegister($domain);
    }
}