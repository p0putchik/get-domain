<?php

namespace Otus\Interfaces;


interface ProxyInterface
{
    public function getIp(): string;

    public function getPort(): int;

    public function getLogin() : string;

    public function getPassword(): string;
}