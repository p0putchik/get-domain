<?php

use Otus\Services\LoggerService;
use PHPUnit\Framework\TestCase;

class LoggerServiceTest extends TestCase
{
    protected $container;
    protected $loggerService;
    protected $domain;

    public function setUp()
    {
        $this->container = $GLOBALS['containerForPHPUnit'];
        $this->loggerService = $this->container->get(LoggerService::class);

        $this->domain = sprintf('a%s.info', md5(uniqid(mt_rand(), true)));
    }

    public function testWriteToLog(): void
    {
        $result = $this->loggerService->writeToDomainRegisterLog('Test');
        $this->assertEquals(true, $result);

        $result = $this->loggerService->writeToDebugLog('Test', true);
        $this->assertEquals(true, $result);
    }

}