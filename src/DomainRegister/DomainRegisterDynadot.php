<?php

namespace Otus\DomainRegister;


use Otus\DomainRegister\Registrators\DomainRegistratorDynadot;
use Otus\Interfaces\DomainRegisterInterface;
use Otus\Services\ConfigService;

class DomainRegisterDynadot implements DomainRegisterInterface
{
    protected $configService;
    protected $registratorDynadot;

    /**
     * DomainAvailabilityCheckerDynadot constructor.
     * @param ConfigService $configService
     * @param DomainRegistratorDynadot $registratorDynadot
     */
    public function __construct(ConfigService $configService, DomainRegistratorDynadot $registratorDynadot)
    {
        $this->configService = $configService;
        $this->registratorDynadot = $registratorDynadot;
    }

    /**
     * @throws \Otus\Exceptions\GetConfigParamException
     */
    public function init(): void
    {
        $paramsArray = [
            'apiKey' => $this->configService->getParam('apiKey', 'dynadot'),
            'apiHost' => $this->configService->getParam('apiHost', 'dynadot')
        ];

        $this->registratorDynadot->init($paramsArray);
    }

    public function registerDomain(string $domain): bool
    {
        return $this->registratorDynadot->domainRegister($domain);
    }
}