<?php

namespace Otus\DomainRegister\Registrators;

use Otus\Exceptions\DomainCheckException;
use Otus\Interfaces\DomainRegistratorInterface;

class DomainRegistratorNamecheap implements DomainRegistratorInterface
{
    protected $apiUserName;
    protected $apiKey;
    protected $userName;
    protected $clientIp;
    protected $sandbox = false;

    /**
     * @param array $configArray
     */
    public function init(array $configArray): void
    {
        foreach ($configArray as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Check domain availability via registrator
     * @param string $domain
     * @return bool
     * @throws DomainCheckException
     */
    public function domainCheckAvailability(string $domain): bool
    {
        $sandbox = $this->sandbox ? '.sandbox' : '';

        $query = sprintf('https://api%s.namecheap.com/xml.response?ApiUser=%s&ApiKey=%s&UserName=%s&Command=namecheap.domains.check&ClientIp=%s&DomainList=%s',
            $sandbox,
            $this->apiUserName,
            $this->apiKey,
            $this->userName,
            $this->clientIp,
            $domain);

        $response = file_get_contents($query);
        if (!$this->validateCheckResponse($response)) {
            throw new DomainCheckException(sprintf('Something wrong in check response: %s', $response));
        }

        //
        $pattern = '/Available=\"(.*?)\"/';
        preg_match($pattern, $response, $match);

        if (empty($match[1])) {
            throw new DomainCheckException(sprintf('Something wrong in check response: %s', $response));
        }

        $checkResult = $match[1];

        switch ($checkResult) {
            case 'true':
                return true;
            case 'false':
                return false;
        }

        throw new DomainCheckException(sprintf('Something wrong in check response result: %s', $response));
    }

    /**
     * Is valid response?
     * @param $response
     * @return bool
     * @throws DomainCheckException
     */
    private function validateCheckResponse($response): bool
    {
        if (empty($response)) {
            throw new DomainCheckException('Empty response from domain check via registrator');
        }

        //try find error message
        $pattern = "/>(.*?)<\/Error>/";
        preg_match($pattern, $response, $match);
        if (!empty($match[1])) {
            throw new DomainCheckException(sprintf('Domain check failed with error: %s', $match[1]));
        }

        if (strpos($response, 'Available=') !== false) {
            return true;
        }

        return false;
    }

    /**
     * Register domain
     * @param string $domain
     * @return bool
     * @throws DomainCheckException
     * @internal param bool $sandbox Test registration
     */
    public function domainRegister(string $domain): bool
    {
        $sandbox = $this->sandbox ? '.sandbox' : '';

        $query = sprintf('https://api%s.namecheap.com/xml.response?ApiUser=%s&ApiKey=%s&UserName=%s&Command=namecheap.domains.create&ClientIp=%s&DomainName=%s&%s',
            $sandbox,
            $this->apiUserName,
            $this->apiKey,
            $this->userName,
            $this->clientIp,
            $domain,
            $this->getRegisterParams()
        );

        $response = file_get_contents($query);
        if (!$this->validateRegisterResponse($response)) {
            throw new DomainCheckException(sprintf('Something wrong in register response: %s', $response));
        }

        return true;
    }

    private function getRegisterParams()
    {
        $fname = 'Ivan';
        $lname = 'Petrov';
        $city = 'Denver';
        $state = 'Colorado';
        $postCode = '10035';
        $country = 'US';
        $phone = '+1.343423232';
        $email = 'sden81@yandex.ru';
        $orgName = 'NC';
        $address = 'S.+Peoria+Street+120-12';

        $paramsArray = [
            'Years' => 1,

            'AuxBillingFirstName' => $fname,
            'AuxBillingLastName' => $lname,
            'AuxBillingAddress1' => $address,
            'AuxBillingStateProvince' => $state,
            'AuxBillingPostalCode' => $postCode,
            'AuxBillingCountry' => $country,
            'AuxBillingPhone' => $phone,
            'AuxBillingEmailAddress' => $email,
            'AuxBillingOrganizationName' => $orgName,
            'AuxBillingCity' => $city,

            'TechFirstName' => $fname,
            'TechLastName' => $lname,
            'TechAddress1' => $address,
            'TechStateProvince' => $state,
            'TechPostalCode' => $postCode,
            'TechCountry' => $country,
            'TechPhone' => $phone,
            'TechEmailAddress' => $email,
            'TechOrganizationName' => $orgName,
            'TechCity' => $city,

            'AdminFirstName' => $fname,
            'AdminLastName' => $lname,
            'AdminAddress1' => $address,
            'AdminStateProvince' => $state,
            'AdminPostalCode' => $postCode,
            'AdminCountry' => $country,
            'AdminPhone' => $phone,
            'AdminEmailAddress' => $email,
            'AdminOrganizationName' => $orgName,
            'AdminCity' => $city,

            'RegistrantFirstName' => $fname,
            'RegistrantLastName' => $lname,
            'RegistrantAddress1' => $address,
            'RegistrantStateProvince' => $state,
            'RegistrantPostalCode' => $postCode,
            'RegistrantCountry' => $country,
            'RegistrantPhone' => $phone,
            'RegistrantEmailAddress' => $email,
            'RegistrantOrganizationName' => $orgName,
            'RegistrantCity' => $city,

            'AddFreeWhoisguard' => 'no',
            'WGEnabled' => 'no',
            'GenerateAdminOrderRefId' => 'False',
            'IsPremiumDomain' => 'False',
        ];

        return http_build_query($paramsArray, '', '&', PHP_QUERY_RFC3986);
    }

    /**
     * @param $response
     * @return bool
     * @throws DomainCheckException
     */
    private function validateRegisterResponse($response): bool
    {
        if (empty($response)) {
            throw new DomainCheckException('Empty response from domain register via registrator');
        }

        //try find error message
        $pattern = "/>(.*?)<\/Error>/";
        preg_match($pattern, $response, $match);
        if (!empty($match[1])) {
            throw new DomainCheckException(sprintf('Domain register failed with error: %s', $match[1]));
        }

        if (strpos($response, 'Registered="true"') !== false) {
            return true;
        }

        return false;
    }
}


