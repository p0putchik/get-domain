<?php

use Otus\TaskManager\TaskManagerViaRabbitMq;
use PHPUnit\Framework\TestCase;

//./vendor/bin/phpunit --bootstrap ./bootstrap/autoload.php tests/TaskManager/

class TaskManagerTest extends TestCase
{
    public function testCheckConnectionToRabbitMQ(): void
    {
        $host = 'localhost';
        $port = 5672;
        $login = 'guest';
        $pass = 'guest';
        $instance = new TaskManagerViaRabbitMq($host, $port, $login, $pass);
        $channel = $instance->connectRabbitMQ();

        $this->assertInstanceOf('PhpAmqpLib\Channel\AMQPChannel', $channel);
    }
}