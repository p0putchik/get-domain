<?php

namespace Otus\DomainAvailabilityChecker;

use Otus\Interfaces\DomainAvailabilityCheckerInterface;
use Otus\Interfaces\DomainRegistratorInterface;
use Otus\Services\ConfigService;

class DomainAvailabilityCheckerNamecheap implements DomainAvailabilityCheckerInterface
{
    protected $configService;
    protected $registratorNamecheap;

    /**
     * DomainAvailabilityCheckerDynadot constructor.
     * @param ConfigService $configService
     * @param DomainRegistratorInterface $registratorNamecheap
     */
    public function __construct(ConfigService $configService, DomainRegistratorInterface $registratorNamecheap)
    {
        $this->configService = $configService;
        $this->registratorNamecheap = $registratorNamecheap;
    }

    /**
     * @param bool $isSandbox
     * @throws \Otus\Exceptions\GetConfigParamException
     */
    public function init(bool $isSandbox = false): void
    {
        $configDir = $isSandbox ? 'namecheapSandbox' : 'namecheap';

        $paramsArray = [
            'apiKey' => $this->configService->getParam('apiKey', $configDir),
            'apiUserName' => $this->configService->getParam('apiUserName', $configDir),
            'userName' => $this->configService->getParam('userName', $configDir),
            'clientIp' => $this->configService->getParam('clientIp', $configDir),
            'sandbox' => $isSandbox,
        ];

        $this->registratorNamecheap->init($paramsArray);
    }

    /**
     * @param string $domain
     * @return bool
     * @throws \Otus\Exceptions\DomainCheckException
     */
    public function isAvailable(string $domain): bool
    {
        return $this->registratorNamecheap->domainCheckAvailability($domain);
    }
}