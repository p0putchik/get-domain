<?php

namespace Otus\Interfaces;

interface DomainRepositoryInterface
{

    /**
     * Add domain
     *
     * @param string $domainName
     * @return mixed
     */
    public function addDomain(string $domainName, string $startDateTime, string $endDateTime);

    /**
     * Receive domain and timing data
     *
     * @return bool
     */
    public function receiveDomainInfo():bool;

    /**
     * Acknowledge received domain
     *
     * @return mixed
     */
    public function ackReceiveDomain();

    /**
     * @return string
     */
    public function getDomainName():string;

    /**
     * @return string
     */
    public function getStartDateTime():string;

    /**
     * @return string
     */
    public function getEndDateTime():string;
}