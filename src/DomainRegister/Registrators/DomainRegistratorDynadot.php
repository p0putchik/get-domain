<?php

namespace Otus\DomainRegister\Registrators;

use Otus\Exceptions\DomainCheckException;
use Otus\Interfaces\DomainRegistratorInterface;

class DomainRegistratorDynadot implements DomainRegistratorInterface
{
    protected $apiKey;
    protected $apiHost;

    /**
     * @param array $configArray
     */
    public function init(array $configArray): void
    {
        foreach ($configArray as $key => $value) {
            $this->$key = $value;
        }
    }


    /**
     * Check domain availability via registrator
     * @param string $domains
     * @return bool
     * @throws DomainCheckException
     */
    public function domainCheckAvailability(string $domains): bool
    {
        $domainsArray = explode(',', $domains);

        $index = 0;
        $paramsArray['key'] = $this->apiKey;
        $paramsArray['command'] = 'search';
        foreach ($domainsArray as $domain) {
            if (!empty($domain)) {
                $paramsArray[sprintf('domain%d', $index++)] = $domain;
            }
        }

        $url = sprintf('%s?%s', $this->apiHost, http_build_query($paramsArray));

        $response = file_get_contents($url);
        if (!$this->validateResponse($response, 'check')) {
            throw new DomainCheckException(sprintf('Something wrong in check response: %s', $response));
        }

        //check result
        $pattern = '/,(.*?),,(.*?),/';
        $searchString = 'yes';
        preg_match_all($pattern, $response, $matches);

        if (empty($matches)) {
            return false;
        }

        foreach ($matches[2] as $match) {
            if (!empty($match) && $match === $searchString) {
                return true;
            }
        }

        return false;
    }

    /**
     * Register domain
     * @param string $domain
     * @return bool
     * @throws DomainCheckException
     * @internal param bool $sandbox Test registration
     */
    public function domainRegister(string $domain): bool
    {
        $paramsArray['key'] = $this->apiKey;
        $paramsArray['command'] = 'register';
        $paramsArray['domain'] = $domain;
        $paramsArray['duration'] = 1;

        $url = sprintf('%s?%s', $this->apiHost, http_build_query($paramsArray));

        $response = file_get_contents($url);
        if (!$this->validateResponse($response, 'register')) {
            throw new DomainCheckException(sprintf('Something wrong in register response: %s', $response));
        }

        if (strpos($response, 'success') !== false) {
            return true;
        }

        return false;
    }

    /**
     * Is valid response?
     * @param string $response
     * @param string $command
     * @return bool
     * @throws DomainCheckException
     */
    private function validateResponse(string $response, string $command): bool
    {
        if (empty($response)) {
            throw new DomainCheckException(sprintf('Empty response from domain %s via registrator', $command));
        }

        //try find error message
        $pattern = "/error|not_available/";
        preg_match($pattern, $response, $match);
        if (!empty($match[0])) {
            throw new DomainCheckException(sprintf('Domain %s failed with error: %s', $command, $match[0]));
        }

        return true;
    }
}


