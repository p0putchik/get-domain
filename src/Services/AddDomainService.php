<?php

namespace Otus\Services;

use Otus\Interfaces\DomainRepositoryInterface;

class AddDomainService
{
    private $domainRepository;

    /**
     * AddDomainService constructor.
     * @param $domainRepository
     */
    public function __construct(DomainRepositoryInterface $domainRepository)
    {
        $this->domainRepository = $domainRepository;
    }

    /**
     * @param string $domainsName
     * @param string $startDateTime
     * @param string $endDateTime
     */
    public function addDomains(string $domainsName, string $startDateTime, string $endDateTime): void
    {
        $domains = explode("\r\n",$domainsName);
        foreach ($domains as $domain){
            $this->domainRepository->addDomain($domain, $startDateTime, $endDateTime);
        }
    }

}