<?php

namespace Otus\Services;

use Otus\Exceptions\GetConfigParamException;
use PHPUnit\Runner\Exception;
use Symfony\Component\Yaml\Yaml;

class ConfigService
{
    protected $configFile;
    protected $secretConfigFile;
    protected $configArray;

    /**
     * ConfigService constructor.
     *
     * @param string $configFile
     * @param null $secretConfigFile
     */
    public function __construct($configFile = "config.yaml", $secretConfigFile = null)
    {
        $this->configFile = $configFile;
        $this->secretConfigFile = $secretConfigFile;
    }

    /**
     * Load config file
     */
    public function loadConfig(): void
    {
        $path = sprintf('%s/../../', __DIR__);

        $configFile = $path . $this->configFile;
        $this->configArray = Yaml::parseFile($configFile);

        if (!empty($this->secretConfigFile)){
            $secretConfigFile = $path . $this->secretConfigFile;
            $secretConfigArray = Yaml::parseFile($secretConfigFile);

            $this->configArray = array_merge($this->configArray, $secretConfigArray);
        }
    }

    /**
     *  Get param from config
     *
     * @param string $paramName
     * @param string $dirName
     * @return mixed
     * @throws GetConfigParamException
     */
    public function getParam(string $paramName, string $dirName = '')
    {
        try {
            return !empty($dirName) ? $this->configArray[$dirName][$paramName] : $this->configArray[$paramName];
        } catch (Exception $e) {
            throw new GetConfigParamException(sprintf('Failed to get param %s from config file %s', $paramName, $this->configFile));
        }
    }
}