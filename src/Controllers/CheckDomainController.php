<?php

namespace Otus\Controllers;

use Otus\Core\Response;
use Otus\Interfaces\ControllerInterface;
use Otus\Interfaces\RequestInterface;
use Otus\Interfaces\ResponseInterface;
use Otus\Services\CheckDomainService;

class CheckDomainController implements ControllerInterface
{
    private $service;

    public function __construct(CheckDomainService $service)
    {
        $this->service = $service;
    }

    /**
     * Executes request and returns response
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws \Otus\Exceptions\DomainCheckException
     */
    public function execute(RequestInterface $request): ResponseInterface
    {
//        $domain = $request->getParam('domain') ?? $this->service->getDomainForHandling();
        $domain = $request->getParam('domain');
        $checker = $request->getParam('checker');

        if (empty($domain)) {
            exit('Empty domain');
        }

        $result = $this->service
            ->setChecker($checker)
            ->isDomainAvailable($domain);

        $response = new Response(sprintf('Check domain %s, result: %s ', $domain, $result));

        return $response;
    }
}