<?php

namespace Otus\DomainRegister;

use Exception;
use Otus\Exceptions\DomainRegisterException;
use Otus\Interfaces\DomainRegisterInterface;
use Otus\Interfaces\DomainRegistratorInterface;
use Otus\Services\ConfigService;


/**
 * @deprecated
 * */
class DomainRegister implements DomainRegisterInterface
{

    protected $domainRegistrator;
    protected $configService;
    protected $isSandbox;

    /**
     * DomainRegister constructor.
     * @param DomainRegistratorInterface $domainRegistrator
     * @param ConfigService $configService
     */
    public function __construct(DomainRegistratorInterface $domainRegistrator, ConfigService $configService)
    {
        $this->configService = $configService;
        $this->domainRegistrator = $domainRegistrator;
    }

    /**
     * @param bool $isSandbox
     * @throws DomainRegisterException
     */
    public function init(bool $isSandbox): void
    {
        $this->isSandbox = $isSandbox;

        $dirNameInConfig = (!$this->isSandbox) ? 'namecheap' : 'namecheapSandbox';

        try {
            $apiUserName = $this->configService->getParam('apiUserName', $dirNameInConfig);
            $apiKey = $this->configService->getParam('apiKey', $dirNameInConfig);
            $userName = $this->configService->getParam('userName', $dirNameInConfig);
            $clientIp = $this->configService->getParam('clientIp', $dirNameInConfig);
        } catch (\Exception $e) {
            throw new DomainRegisterException(sprintf('Domain Register Init Error -> %s', $e->getMessage()));
        }

        $nameCheapConfigArray = [
            'apiUserName' => $apiUserName,
            'apiKey' => $apiKey,
            'userName' => $userName,
            'clientIp' => $clientIp,
            'sandbox' => $isSandbox
        ];

        $this->domainRegistrator->init($nameCheapConfigArray);
    }

    public function registerDomain(string $domain): bool
    {
//        try {
//            $isDomainAvailable = $this->domainRegistrator->domainCheckAvailability($domain);
//        } catch (Exception $e) {
//            throw new DomainRegisterException(sprintf('Domain register Exception -> %s', $e->getMessage()));
//        }

        try {
            $resultRegister = $this->domainRegistrator->domainRegister($domain, $this->isSandbox);
        } catch (Exception $e) {
            throw new DomainRegisterException(sprintf('Domain register Exception -> %s', $e->getMessage()));
        }

        return $resultRegister;
    }
}