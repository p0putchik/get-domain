<?php

use Otus\Services\CheckDomainService;
use PHPUnit\Framework\TestCase;

class CheckDomainServiceTest extends TestCase
{
    protected $container;
    protected $service;
    protected $domain;

    public function setUp()
    {
        $this->container = $GLOBALS['containerForPHPUnit'];
        $this->service = $this->container->get(CheckDomainService::class);

        //change definition for UnitTests
        $this->container->set('RegistratorSandbox', true);

        $this->domain = sprintf('a%s.info', md5(uniqid(mt_rand(), true)));
//        $this->domain = "test.info";
    }


    /**
     * @dataProvider additionProvider
     * @param $checker
     */
    public function testCheckDomainAvailability($checker): void
    {
        $result = $this->service->setChecker($checker)->isDomainAvailable($this->domain);
        $this->assertEquals(true, $result);
    }

    /**
     * @return array
     */
    public function additionProvider(): array
    {
        return [
            'publicWhois' => ['publicWhois'],
            'namecheap' => ['namecheap'],
            'dynadot' => ['dynadot'],
        ];
    }

}