<?php

namespace Otus\Core;


use Otus\Interfaces\RequestInterface;

class Request implements RequestInterface
{
    /**
     * @var array
     */
    private $get;
    /**
     * @var array
     */
    private $post;

    /**
     * Requst constructor.
     * @param array $get
     * @param array $post
     */
    public function __construct(array $get, array $post)
    {
        $this->get = $get;
        $this->post = $post;

        //TODO перенести в PHPDI
        $this->init();
    }

    /**
     *
     */
    public function init()
    {
        $isCLI = (PHP_SAPI === 'cli');

        //for console app
        if ($isCLI) {
            $params = array(
                'd::' => 'domain::',
                'c::' => 'checker::',
                'a:' => 'action:',
                '' => 'help'
            );
//            $action = "handleDomain";
            $errors = [];
            $options = getopt(implode('', array_keys($params)), $params);

            if (isset($options['action']) || isset($options['a'])) {
                $action = $options['action'] ?? $options['a'];
                $this->post['action'] = $action;
            } else {
                $errors[] = 'action required';
            }

            if (isset($options['checker']) || isset($options['c'])) {
                $checker = $options['checker'] ?? $options['c'];
                $this->post['checker'] = $checker;
            }

            if (isset($options['registrator']) || isset($options['r'])) {
                $registrator = $options['registrator'] ?? $options['r'];
                $this->post['registrator'] = $registrator;
            }

            if (isset($options['domain']) || isset($options['d'])) {
                $domain = $options['domain'] ?? $options['d'];
                $this->post['domain'] = $domain;
            }

            if (isset($options['help']) || count($errors)) {
                $help = "
                    usage: php backup.php [--help] [-h|--host=127.0.0.1] [-u|--user=root] [-p|--password=secret] [-d|--database]
                    
                    Options:
                                --help         Show this message
                            -a  --action       Action ('handleDomain', 'addDomainView', 'checkDomain', 'registerDomain')
                            -d  --domain       Domain name
                            -c  --checker      Domain checker ('publicWhois','dynadot', 'namecheap')
                            -r  --registrator  Domain registrator ('dynadot', 'namecheap')
                    Example:
                            php index.php --action=registerDomain --domain=test.info --registrator=namecheap
                            php index.php --action=checkDomain --domain=test.info --registrator=dynadot
                    ";
                if ($errors) {
                    $help .= PHP_EOL . 'Errors:' . PHP_EOL . implode("\n", $errors) . PHP_EOL;
                }
                die($help);
            }
        }
    }

    /**
     * Get param if exist or default value
     *
     * @param string $key
     * @param string|null $default
     * @return null|string
     */
    public function getParam(string $key, string $default = null): ?string
    {
        if ($key === 'action' && isset($_SERVER['REQUEST_URI'])) {
            if (strpos($_SERVER['REQUEST_URI'], 'add') !== false) {
                return 'addDomainView';
            }

            if (strpos($_SERVER['REQUEST_URI'], 'cron') !== false || strpos($_SERVER['REQUEST_URI'], 'handle') !== false) {
                return 'handleDomain';
            }
        }

        if (!empty($this->get[$key])) {
            return $this->get[$key];
        }

        if (!empty($this->post[$key])) {
            return $this->post[$key];
        }

        return $default;
    }
}