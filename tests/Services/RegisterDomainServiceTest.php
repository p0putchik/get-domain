<?php

use Otus\Services\RegisterDomainService;
use PHPUnit\Framework\TestCase;

class RegisterDomainServiceTest extends TestCase
{
    protected $container;
    protected $service;
    protected $domain;

    public function setUp()
    {
        $this->container = $GLOBALS['containerForPHPUnit'];
        $this->service = $this->container->get(RegisterDomainService::class);

        //change definition for UnitTests
        $this->container->set('RegistratorSandbox', true);

        $this->domain = sprintf('a%s.info', md5(uniqid(mt_rand(), true)));
        $this->domain = "test.info";
    }


    /**
     * @dataProvider additionProvider
     * @param $checker
     */
    public function testRegisterDomainAvailability($checker): void
    {
        $result = $this->service->setRegistrator($checker)->registerDomain($this->domain);
        $this->assertEquals(true, $result);
    }

    /**
     * @return array
     */
    public function additionProvider(): array
    {
        return [
//            'namecheap' => ['namecheap'],
            'dynadot' => ['dynadot'],
        ];
    }

}