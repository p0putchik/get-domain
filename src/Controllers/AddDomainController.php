<?php

namespace Otus\Controllers;

use Otus\Core\Response;
use Otus\Exceptions\EmptyAddedDomainException;
use Otus\Interfaces\ControllerInterface;
use Otus\Interfaces\RequestInterface;
use Otus\Interfaces\ResponseInterface;
use Otus\Services\AddDomainService;
use Otus\Services\AddDomainValidationService;
use Otus\View\AddDomainView;

class AddDomainController implements ControllerInterface
{
    private $service;
    private $serviceValidator;
    private $addDomainView;

    /**
     * AddDomainController constructor.
     * @param AddDomainService $service
     * @param AddDomainValidationService $serviceValidator
     * @param AddDomainView $addDomainView
     */
    public function __construct(AddDomainService $service, AddDomainValidationService $serviceValidator, AddDomainView $addDomainView)
    {
        $this->service = $service;
        $this->serviceValidator = $serviceValidator;
        $this->addDomainView = $addDomainView;
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function execute(RequestInterface $request): ResponseInterface
    {
        $addedDomain = $request->getParam('domain');
        $startDateTime = base64_decode($request->getParam('start'));
        $endDateTime = base64_decode($request->getParam('end'));
        $duration = base64_decode($request->getParam('duration'));

        $viewArray = [];
        if (!empty($addedDomain)) {
            $timingArray = $this->serviceValidator->checkTimingForAddedDomain($startDateTime, $endDateTime, $duration);
            $startDateTime = $timingArray['start'];
            $endDateTime = $timingArray['end'];

            $this->service->addDomains($addedDomain, $startDateTime->format('Y-m-d H:i:s'), $endDateTime->format('Y-m-d H:i:s'));
            $viewArray['message'] = 'Domains added to query';
        }
        $content = $this->addDomainView->render($viewArray);

        $response = new Response($content);

        return $response;
    }
}