<?php

use Otus\DomainAvailabilityChecker\DomainAvailabilityCheckerDynadot;
use PHPUnit\Framework\TestCase;

class DomainAvailabilityCheckerDynadotTest extends TestCase
{
    protected $container;
    protected $checker;
    protected $domain;

    public function setUp()
    {
        $this->container = $GLOBALS['containerForPHPUnit'];
        $this->checker = $this->container->get(DomainAvailabilityCheckerDynadot::class);

        $this->domain = sprintf('a%s.info', md5(uniqid(mt_rand(), true)));
        $this->domain = 'test.info';
    }

    public function testChecker(): void
    {
        $result = $this->checker->isAvailable($this->domain);
        $this->assertEquals(true, $result);
    }
}