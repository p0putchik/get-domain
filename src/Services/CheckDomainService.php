<?php

//бисблиотека с проксей
//http://danil.me/php-whois-queries-using-proxy/

namespace Otus\Services;

use DateTime;
use Otus\Exceptions\DomainCheckException;

class CheckDomainService
{
    protected $checkerName;
    protected $checkersArray;
    protected $configService;
    protected $loggerService;

    /**
     * CheckDomain constructor.
     * @param array $checkersArray
     * @param ConfigService $configService
     * @param LoggerService $loggerService
     * @internal param array $checkers
     * @internal param $domainAvailability
     */
    public function __construct(array $checkersArray, ConfigService $configService, LoggerService $loggerService)
    {
        $this->checkersArray = $checkersArray;
        $this->configService = $configService;
        $this->loggerService = $loggerService;
    }

    /**
     * @param $checkerName
     * @return $this|CheckDomainService
     */
    public function setChecker ($checkerName): self
    {
        $this->checkerName = $checkerName;

        return $this;
    }

    protected function getChecker()
    {
        $defaultCheckerName = $this->configService->getParam('defaultChecker', 'checker');

        $checker = $this->checkersArray[$this->checkerName] ?? $this->checkersArray[$defaultCheckerName];

        if (empty($checker)) {
            throw new DomainCheckException('Empty domain checker');
        }

        return $checker;
    }

    public function isDomainAvailable($domain): bool
    {
        $TTL = $this->configService->getParam('TTL(in minutes)', 'checker');
        $sleepTime = $this->configService->getParam('sleep(in seconds)', 'checker');
        $debugMode = (bool)$this->configService->getParam('debug', 'checker');

        $startTime = new DateTime('now');
        $duration = sprintf('+%d minutes', $TTL);
        $endTime = clone $startTime;
        $endTime = $endTime->modify($duration);

        if (empty($TTL)) {
            throw new DomainCheckException('Empty TTL');
        }

        while (true) {
            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $momentStart = $time;

            $currentTime = new DateTime('now');

            if ($currentTime > $endTime) {
                $this->loggerService->writeToDebugLog(sprintf('%s - check timeout', $domain), $debugMode);
                return false;
            }

            $result = false;
            try {
                $checker = $this->getChecker();
                $result = $checker->isAvailable($domain);
            } catch (\Exception $e) {
                $this->loggerService->writeToDebugLog(sprintf('%s - check error:  %s', $domain, $e->getMessage()), $debugMode);
            }

            if ($result){
                $this->loggerService->writeToDebugLog(sprintf('%s - domain can register', $domain), $debugMode);
                echo 'Domain can register' . PHP_EOL;
                return true;
            }

            if ($sleepTime) {
                sleep($sleepTime);
            }

            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $momentFinish = $time;
            $totalTime = round($momentFinish - $momentStart, 4);
            $totalTime = round($totalTime / 0.001);

            if (!$result) {
                echo sprintf('.(%sms)', $totalTime);
            }
        }

        return false;
    }
}