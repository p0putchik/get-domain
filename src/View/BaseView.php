<?php

namespace Otus\View;


use Twig_Environment;
use Twig_Loader_Filesystem;

abstract class BaseView
{
    protected $templateName;
    protected $templateDir;
    private $twig;

    public function __construct(string $templateDir, string $templateName)
    {
        $this->templateName = $templateName;
        $this->templateDir = $templateDir;
    }

    public function init(): void
    {
        $loader = new Twig_Loader_Filesystem(sprintf('%s/%s',__DIR__, $this->templateDir));
        $this->twig = new Twig_Environment($loader);
    }

    public function render(array $inputVarArray){
        $template = $this->twig->load($this->templateName);

        return $template->render($inputVarArray);
    }
}