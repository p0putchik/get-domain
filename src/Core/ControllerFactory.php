<?php

namespace Otus\Core;

use Otus\Exceptions\ControllerNotFoundException;
use Otus\Interfaces\ControllerFactoryInterface;
use Otus\Interfaces\ControllerInterface;
use Otus\Interfaces\RequestInterface;

class ControllerFactory implements ControllerFactoryInterface
{
    /**
     * @var array
     */
    private $routes;

    /**
     * ControllerFactory constructor.
     * @param array $routes
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @param RequestInterface $request
     * @return ControllerInterface
     * @throws ControllerNotFoundException
     */
    public function getController(RequestInterface $request): ControllerInterface
    {
        $filter = $request->getParam('action','addDomainView');
        if (empty($filter) || empty($this->routes[$filter])) {
            throw new ControllerNotFoundException('Invalid Controller');
        }

        return $this->routes[$filter];
    }
}