<?php

namespace Otus\Core;


use Otus\Interfaces\ResponseInterface;

class Response implements ResponseInterface
{
    private $content;

    /**
     * Response constructor.
     * @param string $content
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getResponse():string
    {
        return $this->content;
    }
}