<?php

namespace Otus\Controllers;

use Otus\Core\Response;
use Otus\Interfaces\ControllerInterface;
use Otus\Interfaces\RequestInterface;
use Otus\Interfaces\ResponseInterface;
use Otus\Services\RegisterDomainService;

class RegisterDomainController implements ControllerInterface
{
    private $service;

    public function __construct(RegisterDomainService $service)
    {
        $this->service = $service;
    }

    /**
     * Executes request and returns response
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws \Otus\Exceptions\DomainRegisterException
     * @throws \Otus\Exceptions\GetConfigParamException
     */
    public function execute(RequestInterface $request): ResponseInterface
    {
//        $domain = $request->getParam('domain') ?? $this->service->getDomainForHandling();
        $domain = $request->getParam('domain');
        $registrator = $request->getParam('registrator');

        if (empty($domain)) {
            exit('Empty domain');
        }

        $result = $this->service
            ->setRegistrator($registrator)
            ->registerDomain($domain);

        $response = new Response(sprintf('Try register domain %s, result: %s ', $domain, $result));

        return $response;
    }
}