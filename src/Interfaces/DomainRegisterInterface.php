<?php

namespace Otus\Interfaces;


interface DomainRegisterInterface
{
    public function registerDomain(string $domain):bool;
}