<?php
//http://localhost:15672/#/queues
//http://steffen-rumpf.info:15672/#/queues
//admin/kbytqrf

//https://gist.github.com/sdieunidou/1813409ddfd0185c82c7
//rabbitmqctl add_user test test
//rabbitmqctl set_user_tags test administrator
//rabbitmqctl set_permissions -p / test ".*" ".*" ".*"


namespace Otus\Core;


use Otus\Exeptions\FailedConnectionRabbitMQ;
use Otus\Services\ConfigService;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQ
{
    private $host;
    private $port;
    private $login;
    private $password;
    private $queueName;
    private $connectionInstance;
    private $channelInstance;
    private $message;
    private $configService;

    /**
     * RabbitMQ constructor.
     * @param ConfigService $configService
     * @internal param $host
     * @internal param $port
     * @internal param $login
     * @internal param $password
     * @internal param $queueName
     */

    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
    }

    /**
     *
     */
    public function init(): void
    {
        $this->host = $this->configService->getParam('host','rabbitMQ');
        $this->port = $this->configService->getParam('port','rabbitMQ');
        $this->login = $this->configService->getParam('login','rabbitMQ');
        $this->password = $this->configService->getParam('password','rabbitMQ');
        $this->queueName = $this->configService->getParam('queueName','rabbitMQ');
    }
    /**
     * @return $this
     */
    public function closeConnectionRabbitMQ(): self
    {
        if (!empty($this->connectionInstance) && !empty($this->channelInstance)) {
            $this->channelInstance->close();
            $this->connectionInstance->close();
        }

        return $this;
    }

    /**
     * @param string $messageText
     * @return $this|RabbitMQ
     * @throws FailedConnectionRabbitMQ
     */
    public function sendMessage(string $messageText): self
    {
        if (empty($this->channelInstance)) {
            $this->connectRabbitMQ();
        }

        $this->channelInstance->queue_declare($this->queueName, false, false, false, false);

        $msg = new AMQPMessage($messageText);
        $this->channelInstance->basic_publish($msg, '', $this->queueName);

        return $this;
    }

    /**
     * @return \PhpAmqpLib\Channel\AMQPChannel
     * @throws FailedConnectionRabbitMQ
     */
    public function connectRabbitMQ(): \PhpAmqpLib\Channel\AMQPChannel
    {
        try {
            $this->connectionInstance = new AMQPStreamConnection($this->host, $this->port, $this->login, $this->password);
            $this->channelInstance = $this->connectionInstance->channel();
        } catch (\Exception $e) {
            throw new FailedConnectionRabbitMQ('');
        }

        return $this->channelInstance;
    }

    public function receiveMessage()
    {
        if (empty($this->channelInstance)) {
            $this->connectRabbitMQ();
        }

        $this->channelInstance->queue_declare($this->queueName, false, false, false, false);
        $this->message = $this->channelInstance->basic_get($this->queueName);

        return $this;
    }

    public function getMessageBody(){
        if (!empty($this->message)) {
            return $this->message->body;
        }

        return null;
    }

    public function ackMessage()
    {
        if (!empty($this->message)) {
            $this->channelInstance->basic_ack($this->message->delivery_info['delivery_tag']);
        }

        return;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host): void
    {
        $this->host = $host;
    }

    /**
     * @param mixed $port
     */
    public function setPort($port): void
    {
        $this->port = $port;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login): void
    {
        $this->login = $login;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @param mixed $queueName
     */
    public function setQueueName($queueName): void
    {
        $this->queueName = $queueName;
    }
}