<?php

namespace Otus\DomainAvailabilityChecker;

use Helge\Service\DomainAvailability;
use Otus\Interfaces\DomainAvailabilityCheckerInterface;

class DomainAvailabilityCheckerPublicWhois implements DomainAvailabilityCheckerInterface
{
    protected $whoisChecker;

    /**
     * DomainAvailabilityCheckerPublicWhois constructor.
     * @param DomainAvailability $checker
     */
    public function __construct(DomainAvailability $checker)
    {
        $this->whoisChecker = $checker;
    }


    /**
     * @param string $domain
     * @return bool
     */
    public function isAvailable(string $domain): bool
    {
        return $this->whoisChecker->isAvailable($domain);
    }

}