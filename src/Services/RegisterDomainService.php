<?php

//бисблиотека с проксей
//http://danil.me/php-whois-queries-using-proxy/

namespace Otus\Services;

use DateTime;
use Otus\Exceptions\DomainRegisterException;

class RegisterDomainService
{
    protected $registratorName;
    protected $registratorsArray;
    protected $configService;
    protected $loggerService;


    /**
     * RegisterDomainService constructor.
     * @param array $registratorsArray
     * @param ConfigService $configService
     * @param LoggerService $loggerService
     */
    public function __construct(array $registratorsArray, ConfigService $configService, LoggerService $loggerService)
    {
        $this->registratorsArray = $registratorsArray;
        $this->configService = $configService;
        $this->loggerService = $loggerService;
    }

    public function setRegistrator ($registratorName): self
    {
        $this->registratorName = $registratorName;

        return $this;
    }

    /**
     * @return mixed
     * @throws DomainRegisterException
     * @throws \Otus\Exceptions\GetConfigParamException
     */
    protected function getRegistrator()
    {
        $defaultRegistratorName = $this->configService->getParam('defaultRegistrator', 'register');

        $registrator = $this->registratorsArray[$this->registratorName] ?? $this->registratorsArray[$defaultRegistratorName];

        if (empty($registrator)) {
            throw new DomainRegisterException('Empty domain registrator');
        }

        return $registrator;
    }

    /**
     * @param $domain
     * @return bool
     * @throws DomainRegisterException
     * @throws \Otus\Exceptions\GetConfigParamException
     */
    public function registerDomain($domain): bool
    {
        $TTL = $this->configService->getParam('TTL(in minutes)', 'register');
        $sleepTime = $this->configService->getParam('sleep(in seconds)', 'register');
        $debugMode = (bool)$this->configService->getParam('debug', 'register');

        $startTime = new DateTime('now');
        $duration = sprintf('+%d minutes', $TTL);
        $endTime = clone $startTime;
        $endTime = $endTime->modify($duration);

        if (empty($TTL)) {
            throw new DomainRegisterException('Empty TTL');
        }

        while (true) {
            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $momentStart = $time;

            $currentTime = new DateTime('now');

            if ($currentTime > $endTime) {
                $this->loggerService->writeToDebugLog(sprintf('%s - register timeout', $domain), $debugMode);
                return false;
            }

            $result = false;
            try {
                $registrator = $this->getRegistrator();
                $result = $registrator->registerDomain($domain);
            } catch (\Exception $e) {
                $this->loggerService->writeToDebugLog(sprintf('%s - register error:  %s', $domain, $e->getMessage()), $debugMode);
            }

            if ($result){
                $this->loggerService->writeToDebugLog(sprintf('%s - domain can register', $domain), $debugMode);
                echo 'Domain register' . PHP_EOL;
                return true;
            }

            if ($sleepTime) {
                sleep($sleepTime);
            }

            $time = microtime();
            $time = explode(' ', $time);
            $time = $time[1] + $time[0];
            $momentFinish = $time;
            $totalTime = round($momentFinish - $momentStart, 4);
            $totalTime = round($totalTime / 0.001);

            if (!$result) {
                echo sprintf('.(%sms)', $totalTime);
            }
        }

        return false;
    }
}