<?php


namespace Otus\Services;


use DateTime;
use DateTimeZone;

class AddDomainValidationService
{
    /**
     * @param null $startDateTime
     * @param null $endDateTime
     * @param null $duration
     * @return array
     */
    public function checkTimingForAddedDomain($startDateTime = null, $endDateTime = null, $duration = null): array
    {
        if (empty($startDateTime)) {
            $startDateTime = new DateTime('now', new DateTimeZone('Europe/Moscow'));
        } else {
            $startDateTime = new DateTime($startDateTime, new DateTimeZone('Europe/Moscow'));
        }

        if (empty($duration)){
            $duration = '+120 minute';
        }

        if (empty($endDateTime)) {
            $endDateTime = clone $startDateTime;
            $endDateTime = $endDateTime->modify($duration);
        } else {
            $endDateTime = new DateTime($endDateTime, new DateTimeZone('Europe/Moscow'));
        }

        return [
            'start' => $startDateTime,
            'end' => $endDateTime,
        ];
    }
}
