<?php

namespace Otus\Core;

use Otus\Interfaces\RequestBuilderInterface;
use Otus\Interfaces\RequestInterface;

class RequestBuilder implements RequestBuilderInterface
{

    /**
     * @param array $get
     * @param array $post
     * @return RequestInterface
     */
    public function getRequest(array $get, array $post): RequestInterface
    {
        return new Request($get, $post);
    }
}