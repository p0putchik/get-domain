<?php

use Otus\Core\RabbitMQ;
use Otus\Services\AddDomainService;
use PHPUnit\Framework\TestCase;

class AddDomainServiceTest extends TestCase
{
    protected $container;
    protected $addDomainService;
    protected $domain;

    public function setUp()
    {
        $this->container = $GLOBALS['containerForPHPUnit'];
        $this->addDomainService = $this->container->get(AddDomainService::class);
    }

    /**
     * @var $rabbit RabbitMQ
     */
    public function testRabbitMQ()
    {
        $testMessage = "testMessage";

        $rabbit = $this->container->get(RabbitMQ::class);
        $rabbit->closeConnectionRabbitMQ();
        $rabbit->setQueueName("testQueue");
        $rabbit->connectRabbitMQ();
        $rabbit->sendMessage($testMessage);
        $rabbit->receiveMessage();
        $message = $rabbit->getMessageBody();
        $rabbit->ackMessage();

        $this->assertEquals($testMessage, $message);
    }

    public function testAddDomain(): void
    {
        $domain = "ddd.info";
        $startDateTime = new DateTime();
        $startDateTime->modify('-2 day');
        $endDateTime = new DateTime();
        $endDateTime->modify('-1 day');

        $rabbit = $this->container->get(RabbitMQ::class);
        $rabbit->closeConnectionRabbitMQ();
        $rabbit->setQueueName("testQueue");
        $rabbit->connectRabbitMQ();

        $result = $this->addDomainService->addDomains($domain, $startDateTime->format('Y-m-d H:i:s'), $endDateTime->format('Y-m-d H:i:s'));
        $rabbit->receiveMessage();
        $message = $rabbit->getMessageBody();
        $rabbit->ackMessage();

        $this->assertContains($domain, $message);
    }
}