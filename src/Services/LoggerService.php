<?php

namespace Otus\Services;


use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerService
{
    private $configService;
    private $domainRegisterLogger;
    private $debugLogger;

    /**
     * LoggerService constructor.
     * @param $configService
     */
    public function __construct(ConfigService $configService)
    {
        $this->configService = $configService;
    }

    /**
     *
     */
    public function init()
    {
        //set message formater
        // the default date format is "Y-m-d H:i:s"
        $dateFormat = "Y-m-d H:i:s";
        // the default output format is "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
        $output = "%datetime% - %message% \n";
        $formatter = new LineFormatter($output, $dateFormat);
        $fileSizeLimit = 1024 * $this->configService->getParam('fileSizeLimit(in KB)', 'logging');

        $domainRegisterLogFileName = sprintf('%s/../../log/%s', __DIR__, $this->configService->getParam('domainCheckerLogFileName', 'logging'));
        if (!empty($domainRegisterLogFileName)) {
            $this->createEmptyLogFileIfNeed($domainRegisterLogFileName);
            $this->cutLogFileIfNeed($domainRegisterLogFileName, $fileSizeLimit);
            $this->domainRegisterLogger = new Logger('domainChecker');

            $stream = new StreamHandler($domainRegisterLogFileName, Logger::DEBUG);
            $stream->setFormatter($formatter);
            $this->domainRegisterLogger->pushHandler($stream);
        }

        $debugLogFileName = sprintf('%s/../../log/%s', __DIR__, $this->configService->getParam('debugLogFileName', 'logging'));
        if (!empty($debugLogFileName)) {
            $this->createEmptyLogFileIfNeed($debugLogFileName);
            $this->cutLogFileIfNeed($debugLogFileName, $fileSizeLimit);
            $this->debugLogger = new Logger('debug');

            $stream = new StreamHandler($debugLogFileName, Logger::DEBUG);
            $stream->setFormatter($formatter);
            $this->debugLogger->pushHandler($stream);
        }
    }

    public function writeToDomainRegisterLog(string $message)
    {
        return $this->domainRegisterLogger->info($message);
    }

    public function writeToDebugLog(string $message, bool $debug = false)
    {
        return ($debug) ? $this->debugLogger->info($message) : null;
    }

    protected function createEmptyLogFileIfNeed($logFileName)
    {
        if (!file_exists($logFileName)) {
            file_put_contents($logFileName, '');
        }
    }

    protected function cutLogFileIfNeed($logFileName, $limit)
    {
        if (!file_exists($logFileName) || empty($limit)) {
            return false;
        }

        $fileSize = filesize($logFileName);
        if ($fileSize > $limit) {
            //need cut
            $stringArray = file($logFileName);
            if (empty($stringArray) || !is_array($stringArray)) {
                return false;
            }
            $offset = round(count($stringArray) / 1.5);
            $stringArray = array_slice($stringArray, $offset);
            file_put_contents($logFileName, implode("", $stringArray));
        }
    }

}