<?php

namespace Otus\Interfaces;

interface DomainAvailabilityCheckerInterface
{
    public function isAvailable(string $domains): bool;

}