<?php

use DI\ContainerBuilder;
use Helge\Client\WhoisClientInterface;
use Helge\Loader\JsonLoader;
use Helge\Loader\LoaderInterface;
use Otus\Controllers\AddDomainController;
use Otus\Controllers\CheckDomainController;
use Otus\Controllers\DomainHandlerController;
use Otus\Controllers\RegisterDomainController;
use Otus\Core\ControllerFactory;
use Otus\Core\RabbitMQ;
use Otus\Core\RequestBuilder;
use Otus\DomainAvailabilityChecker\DomainAvailabilityCheckerDynadot;
use Otus\DomainAvailabilityChecker\DomainAvailabilityCheckerNamecheap;
use Otus\DomainAvailabilityChecker\DomainAvailabilityCheckerPublicWhois;
use Otus\DomainAvailabilityChecker\ExtendWhoIsClient;
use Otus\DomainAvailabilityChecker\Socks5SocketClient\Client;
use Otus\DomainRegister\DomainRegister;
use Otus\DomainRegister\DomainRegisterDynadot;
use Otus\DomainRegister\DomainRegisterNamecheap;
use Otus\DomainRegister\Registrators\DomainRegistratorDynadot;
use Otus\DomainRegister\Registrators\DomainRegistratorNamecheap;
use Otus\Interfaces\ControllerFactoryInterface;
use Otus\Interfaces\DomainRegisterInterface;
use Otus\Interfaces\DomainRegistratorInterface;
use Otus\Interfaces\DomainRepositoryInterface;
use Otus\Interfaces\ProxyInterface;
use Otus\Interfaces\RequestBuilderInterface;
use Otus\Repositories\DomainRepository;
use Otus\Services\CheckDomainService;
use Otus\Services\ConfigService;
use Otus\Services\LoggerService;
use Otus\Services\ProxyService;
use Otus\Services\RegisterDomainService;
use Otus\View\AddDomainView;

$root = dirname(__DIR__);

require "{$root}/vendor/autoload.php";

$builder = new ContainerBuilder();

$builder->useAutowiring(true);

$builder->addDefinitions([
    RequestBuilderInterface::class => DI\create(RequestBuilder::class),
//    RequestBuilder::class =>DI\autowire()->constructor()->method('init'),

    ControllerFactoryInterface::class => DI\create(ControllerFactory::class)
        ->constructor(DI\get('action')),

    'action' => [
        'addDomainView' => DI\get(AddDomainController::class),
        'handleDomain' => DI\get(DomainHandlerController::class),
        'checkDomain' => DI\get(CheckDomainController::class),
        'registerDomain' => DI\get(RegisterDomainController::class),
    ],

    CheckDomainService::class => DI\create()
        ->constructor(DI\get('domainChecker'), DI\get(ConfigService::class),DI\get(LoggerService::class)),

    'domainChecker' =>[
        'publicWhois' => DI\get(DomainAvailabilityCheckerPublicWhois::class),
        'dynadot' => DI\get(DomainAvailabilityCheckerDynadot::class),
        'namecheap' => DI\get(DomainAvailabilityCheckerNamecheap::class),
    ],

    RegisterDomainService::class => DI\create()
        ->constructor(DI\get('domainRegistrator'), DI\get(ConfigService::class),DI\get(LoggerService::class)),

    'domainRegistrator' =>[
        'namecheap' =>DI\get(DomainRegisterNamecheap::class),
        'dynadot' =>DI\get(DomainRegisterDynadot::class),
    ],

    'template.dir' => 'Templates',
    'template.addDomain' => 'add-domain.html',

    AddDomainView::class => DI\create()
        ->constructor(DI\get('template.dir'), DI\get('template.addDomain'))
        ->method('init'),

    DomainRepositoryInterface::class => DI\autowire(DomainRepository::class)
        ->constructor(DI\get(RabbitMQ::class)),

    ConfigService::class => DI\create()
        ->constructor(DI\get('config.fileName'), DI\get('secretConfig.fileName'))
        ->method('loadConfig'),

    RabbitMQ::class => DI\create()
        ->constructor(DI\get(ConfigService::class))
        ->method('init')
        ->method('connectRabbitMQ'),

    'config.fileName' => 'config.yaml',
    'secretConfig.fileName' => 'secrets.yaml',

    ProxyInterface::class => DI\autowire(ProxyService::class)
        ->constructor(DI\get(ConfigService::class))
        ->method('init'),

    LoggerService::class => DI\create()
        ->constructor(DI\get(ConfigService::class))
        ->method('init'),

    Client::class => DI\create()
        ->constructor(DI\get(ProxyInterface::class))
        ->method('configureProxy'),

    'whoisServersFileName' =>__DIR__.'/../servers.json',
    WhoisClientInterface::class => DI\autowire(ExtendWhoIsClient::class),
    LoaderInterface::class => DI\create(JsonLoader::class)
        ->constructor(DI\get('whoisServersFileName')),


    //check domain
    DomainAvailabilityCheckerDynadot::class => DI\create()
        ->constructor(DI\get(ConfigService::class), DI\get(DomainRegistratorDynadot::class))
        ->method('init')
        ->lazy(),

    'RegistratorSandbox' => false,
    DomainAvailabilityCheckerNamecheap::class => DI\create()
        ->constructor(DI\get(ConfigService::class), DI\get(DomainRegistratorNamecheap::class))
        ->method('init', DI\get('RegistratorSandbox'))
        ->lazy(),

    //register domain
    DomainRegisterInterface::class => DI\autowire(DomainRegister::class)
        ->constructor(DI\get(DomainRegistratorInterface::class), DI\get(ConfigService::class))
        ->method('init', DI\get('RegistratorSandbox'))
        ->lazy(),

    DomainRegisterNamecheap::class => DI\create()
        ->constructor(DI\get(ConfigService::class), DI\get(DomainRegistratorNamecheap::class))
        ->method('init', DI\get('RegistratorSandbox'))
        ->lazy(),

    DomainRegisterDynadot::class => DI\create()
        ->constructor(DI\get(ConfigService::class), DI\get(DomainRegistratorDynadot::class))
        ->method('init')->lazy(),
]);

$container = $builder->build();

$GLOBALS['containerForPHPUnit'] = &$container;
