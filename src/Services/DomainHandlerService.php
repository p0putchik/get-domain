<?php

namespace Otus\Services;

use DateTime;
use Otus\DomainAvailabilityChecker\DomainAvailabilityCheckerInterface;
use Otus\DomainAvailabilityChecker\Socks5SocketClient\Client;
use Otus\Interfaces\DomainRegisterInterface;
use Otus\Interfaces\DomainRepositoryInterface;

class DomainHandlerService
{
    private $domainRepository;
    private $domainCheckService;
    private $domainRegister;
    private $configService;
    private $loggerService;

    /**
     * DomainHandlerService constructor.
     * @param DomainRepositoryInterface $domainRepository
     * @param CheckDomainService $domainCheckService
     * @param DomainRegisterInterface $domainRegister
     * @param ConfigService $configService
     * @param LoggerService $loggerService
     * @internal param DomainAvailabilityCheckerInterface $domainAvailabilityChecker
     */
    public function __construct(
        DomainRepositoryInterface $domainRepository,
        CheckDomainService $domainCheckService,
        DomainRegisterInterface $domainRegister,
        ConfigService $configService,
        LoggerService $loggerService
    )
    {
        $this->domainRepository = $domainRepository;
        $this->domainCheckService = $domainCheckService;
        $this->domainRegister = $domainRegister;
        $this->configService = $configService;
        $this->loggerService = $loggerService;
    }

    /**
     * @return bool|int|null|string
     */
    public function getDomainForHandling()
    {
        //empty queue
        if (!$this->domainRepository->receiveDomainInfo()) {
            return null;
        }

        //receive domain info
        $domainName = $this->domainRepository->getDomainName();
        $startDateTime = new DateTime($this->domainRepository->getStartDateTime());
        $endDateTime = new DateTime($this->domainRepository->getEndDateTime());

        $currentTime = new DateTime();

        //too late, ignore domain and delete from queue
        if ($currentTime > $endDateTime) {
            $this->ackReceiveDomainAfterHandling();
            return 0;
        }

        //too early, ignore domain and read to queue
        if ($currentTime < $startDateTime) {
            return null;
        }

        return $domainName;
    }

    /**
     * Send acknowledgement to rabbitMQ, that handle domain
     */
    public function ackReceiveDomainAfterHandling(): void
    {
        $this->domainRepository->ackReceiveDomain();
    }

    /**
     *
     */
    public function closeConnection(): void
    {
        $this->domainRepository->closeConnection();
    }

    /**
     * @param $domain
     * @return bool
     * @throws \Otus\Exceptions\GetConfigParamException
     */
//    public function checkAndRegisterDomain($domain): bool
//    {
//        printf('Start check domain %s%s',$domain, PHP_EOL);
//        $this->loggerService->writeToDomainRegisterLog(sprintf('%s - start check domain', $domain));
//        $isAvailable = $this->handleDomain($domain, 'checker');
//
//        if ($isAvailable) {
//            $this->loggerService->writeToDomainRegisterLog(sprintf('%s - domain is available', $domain));
//            printf('Start register domain %s%s',$domain, PHP_EOL);
//            $this->loggerService->writeToDomainRegisterLog(sprintf('%s - start register domain', $domain));
//
//            $isRegister = $this->handleDomain($domain, 'register');
//
//            if ($isRegister) {
//                $this->loggerService->writeToDomainRegisterLog(sprintf('%s - domain register', $domain));
//
//                return true;
//            } else {
//                $this->loggerService->writeToDomainRegisterLog(sprintf('%s - can\'t domain register', $domain));
//            }
//        } else {
//            $this->loggerService->writeToDomainRegisterLog(sprintf('%s - end domain check', $domain));
//        }
//
//        return false;
//    }


    /**
     * @param string $domain
     * @param string $action
     * @return bool|null
     * @throws \Otus\Exceptions\GetConfigParamException
     */
//    public function handleDomain(string $domain, string $action): ?bool
//    {
//        if ($action !== 'checker' && $action !== 'register') {
//            exit('wrong action');
//        }
//
//        $TTL = $this->configService->getParam('TTL(in minutes)', $action);
//        $sleepTime = $this->configService->getParam('sleep(in seconds)', $action);
//        $debugMode = (bool)$this->configService->getParam('debug', $action);
//
//        $startTime = new DateTime('now');
//        $duration = sprintf('+%d minutes', $TTL);
//        $endTime = clone $startTime;
//        $endTime = $endTime->modify($duration);
//
//        $doing = ($action === 'checker') ? 'check' : 'register';
//
//        if (!empty($TTL) && ($endTime > $startTime)) {
//            $this->loggerService->writeToDebugLog(sprintf('%s - start %s', $domain, $doing), $debugMode);
//
//            while (true) {
//                $time = microtime();
//                $time = explode(' ', $time);
//                $time = $time[1] + $time[0];
//                $momentStart = $time;
//
//                $startTime = new DateTime('now');
//
//                if ($startTime > $endTime) {
//                    $this->loggerService->writeToDebugLog(sprintf('%s - %s timeout', $domain, $doing), $debugMode);
//                    $this->ackReceiveDomainAfterHandling();
//                    return false;
//                }
//
//                $result = false;
//                if ($action === 'checker') {
//                    try {
//                        $result = $this->domainAvailabilityChecker->isDomainAvailable($domain);
//                    } catch (\Exception $e) {
//                        $this->loggerService->writeToDebugLog(sprintf('%s error in %s for domain: %s', $domain, $doing, $e->getMessage()), $debugMode);
//                    }
//                } else {
//                    try {
//                        $result = $this->domainRegister->registerDomain($domain);
//                    } catch (\Exception $e) {
//                        $this->loggerService->writeToDebugLog(sprintf('%s error in %s for domain: %s', $domain, $doing, $e->getMessage()), $debugMode);
//                    }
//                }
//
//                if ($result) {
//                    if ($action === 'checker') {
//                        $this->loggerService->writeToDebugLog(sprintf('%s - domain can register', $domain), $debugMode);
//                        echo "Domain can register".PHP_EOL;
//                    } else {
//                        echo "Domain register".PHP_EOL;
//                        $this->loggerService->writeToDebugLog(sprintf('%s - domain register', $domain), $debugMode);
//                        $this->ackReceiveDomainAfterHandling();
//                    }
//                    return true;
//                }
//
////                $intermediateLogging = random_int(1, 10) === 3;
////                if ($debugMode && !$result && $intermediateLogging) {
////                    $this->loggerService->writeToDebugLog(sprintf('%s - domain can\'t register yet', $domain), $debugMode);
////                }
//
//                if ($sleepTime){
//                    sleep($sleepTime);
//                }
//
//                $time = microtime();
//                $time = explode(' ', $time);
//                $time = $time[1] + $time[0];
//                $momentFinish = $time;
//                $totalTime = round(($momentFinish - $momentStart), 4);
//                $totalTime = round($totalTime / 0.001);
//
//                if (!$result){
//                    echo sprintf(".(%sms)", $totalTime);
//                }
//            }
//        }
//    }

    public function test()
    {
        $s = new Client(new ProxyService($this->configService));
        $s->configureProxy();
//        $s->configureProxy(array(
//            'hostname' => '185.146.66.133',
//            'port' => 9064,
//            'username' => 'Yh7zpu',
//            'password' => 'xZLBXC',
////            'use_dnstunnel' => true
//
//        ));
        $s->connect('whois.afilias.net', 43);
        $request = "test.info\r\n";
        $s->send($request);
        $response = $s->readAll();
        echo '<h1>The response was:</h1><pre>' . $response . '</pre>';


        $url = "whois.afilias.net";
        $port = 43;
        $proxy = "185.146.66.133:9064";
        $proxy_log_pass = "Yh7zpu:xZLBXC";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT, $port);
//        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy_log_pass);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "test.info" . "\r\n");
        $curl_scraped_page = curl_exec($ch);
        $error = curl_error($ch);
        echo $error;
        echo $curl_scraped_page;
        curl_close($ch);


        $proxy = '13.155.15.165'; // proxy
        $port = 7384; // proxy port

        $fp = fsockopen($proxy, $port); // connect to proxy
        fputs($fp, "CONNECT whois.afilias.net:43\r\n test.info\r\n");

        $data = '';
        while (!feof($fp)) $data .= fgets($fp, 1024);
        fclose($fp);
        var_dump($data);
    }
}