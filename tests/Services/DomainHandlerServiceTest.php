<?php

use Otus\Services\DomainHandlerService;
use PHPUnit\Framework\TestCase;

class DomainHandlerServiceTest extends TestCase
{
    protected $container;
    protected $domainHandlerService;
    protected $domain;

    public function setUp()
    {
        $this->container = $GLOBALS['containerForPHPUnit'];
        $this->domainHandlerService = $this->container->get(DomainHandlerService::class);

        //change definition for UnitTests
        $this->container->set('RegistratorSandbox', true);

//        $this->domain = sprintf('a%s.info', md5(uniqid(mt_rand(), true)));
        $this->domain = "test.info";
    }

    public function testCheckAndRegister(): void
    {
        $result = $this->domainHandlerService->checkAndRegisterDomain($this->domain);
        $this->assertEquals(true, $result);
    }

    public function testCheckDomainAvailability(): void
    {
//        $this->markTestIncomplete('This test has not been implemented yet.');

        return;
        $result = $this->domainHandlerService->checkDomainAvailability($this->domain);
        $this->assertEquals(true, $result);
    }

    public function testDomainRegister(): void
    {
        return;

        $this->markTestSkipped('Skip');

        $result = $this->domainHandlerService->registerDomain($this->domain);
        $this->assertEquals(true, $result);
    }

}